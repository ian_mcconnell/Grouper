# Grouper

Tasks:

Anna- need to create the maze, add assets, and write code for fish sensors

Leo- code for small  fish mobs: idle state, scanning for nearby players, follow players, and destroyed by fisherman
	also code homing ice bombs for players to shoot at each other
	also find and mix audio with Ian
	
Ian- Create fisherman, including: tossing dynamite that destroys nearby fish, casting a line that reels in hit fish, 
    and creating the visual aspect of the fisherman using external resources
    Create FSM for controlling fishrman
    Find and mix audio with Leo

Krystale-Create Poster, Add Assets, Help with stuff, code start, code menu, code stuff.
